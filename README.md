# microservices-in-python
microservices-in-python

Microservices Tutorial

Tools used:
	Python
	Docker
	Dockefile
	Docker-Compose
	Kubernates
	Helm
	Git

Steps:
	Installing Python - Completed 
	Creating Python Virtual ENV - Completed
	Installing Python VS Code Extension - Completed
	Sample Flask Application - Completed
	Jinja templating for Dynamic Web pages - Completed
	Using PIP to freeze Python dependencies - Completed
	Installing minikube - Completed
	Buidling the docker image using Dockerfile
	Writing Docker compose file
	Writing Kubernates manifest files for the application
	Creating Helm Chart


Prereuisites:
	Python 3+
	Docker & Docker-Compose
	Kubernetes Cluster like minikube
	Kuberctl & Heml CLI
	VS Code


Minikube:
brew install qemu
minikube start --driver=qemu
minikube status
kubectl clutser-info
kubectl get nodes
minikube stop

Docker steps:

1: docker build -t webapp:1.0 .     
2: docker images
3: docker run -d -p 80:5000 --name web webapp:1.0
4: docker image rm
5: docker kill 
6: docker ps -a

Kubernete steps:
1: kubectl apply -f deployment.yml
2: kubectl apply -f service.yml
3: kubectl get po
4: kubectl delete -f .


github token:
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